import unicodecsv
import misc


def get_all_from_elvanto(connection, custom_field_ids=[]):

    have_all = False
    page = 1
    page_size = 1000
    people_list = []

    params = {
        'page_size':page_size,
        'fields': [
            'gender',
            'birthday',
            'anniversary',
            'school_grade',
            'marital_status',
            'development_child',
            'special_needs_child',
            'security_code',
            'receipt_name',
            'giving_number',
            'mailing_address',
            'mailing_address2',
            'mailing_city',
            'mailing_state',
            'mailing_postcode',
            'mailing_country',
            'home_address',
            'home_address2',
            'home_city',
            'home_state',
            'home_postcode',
            'home_country',
            'access_permissions',
            'departments',
            'service_types',
            'demographics',
            'locations',
            'reports_to',
            ]
        }

    params['fields'] += ['custom_'+x for x in custom_field_ids]

    while not have_all:

        params['page'] = page
        data = connection._Post("people/getAll", **params)

        if not ('status' in data and 'people' in data):
            return None
        if not data['status'] == "ok":
            return None

        for p in data['people'].get('person',[]):
            people_list.append(p)

        if data['people'].get('total',0) <= page * page_size:
            have_all = True
        else:
            page += 1

    return people_list


def save_to_csv(connection, csv_filepath, custom_fields=[], debug=True):
    
    #Get data from Elvanto
    if debug: print "Downloading people data"
    custom_field_ids = [f[0] for f in custom_fields]
    people_list = get_all_from_elvanto(connection, custom_field_ids)

    if not people_list:
        raise Exception("No people data returned by API")

    #Normalise some fields
    for person in people_list:
        if "locations" in person:
            locations = misc.get_special_list(person,'locations','location')
            for x in locations:
                new_name = "location_" + x.get('name','')
                person[new_name] = True
            person.pop("locations",None)

        if "demographics" in person:
            demographics = misc.get_special_list(person,'demographics','demographic')
            for x in demographics:
                new_name = "demographic_" + x.get('name','')
                person[new_name] = True
            person.pop("demographics",None)

        if "service_types" in person:
            service_types = misc.get_special_list(person,'service_types','service_type')
            for x in service_types:
                new_name = "service_type_" + x.get('name','')
                person[new_name] = True
            person.pop("service_types",None)

        if "access_permissions" in person:
            access_permissions = misc.get_special_list(person,'access_permissions','access_permission')
            for x in access_permissions:
                new_name = "access_permission_" + x.get('name','')
                person[new_name] = True
            person.pop("access_permissions",None)

        if "departments" in person:
            departments = misc.get_special_list(person,'departments','department')
            for d in departments:
                sub_departments = misc.get_special_list(d,'sub_departments','sub_department')
                for sd in sub_departments:
                    positions = misc.get_special_list(sd,'positions','position')
                    for p in positions:
                        new_name = "department_" + d.get('name','') + " - " +\
                            sd.get('name','') + " - " + p.get('name','')
                        person[new_name] = True
            person.pop("departments",None)

        for (field_id, field_name) in custom_fields:
            k = "custom_"+field_id
            if k in person:
                if isinstance(person[k], dict):
                    if 'custom_field' in person[k]:
                        person[field_name] = ", ".join([x['name'] for x in person[k]['custom_field']])
                    else:
                        person[field_name] = person[k]['name']
                else:
                    person[field_name] = person[k]
                person.pop(k,None)

    #Get list of all keys that are used
    keys = []
    for person in people_list:
        for k in person.keys():
            if k not in keys:
                keys.append(k)
    keys.sort()
    #Move ID and name fields to start of key-list
    keys.insert(0, keys.pop(keys.index("lastname")))
    keys.insert(0, keys.pop(keys.index("firstname")))
    keys.insert(0, keys.pop(keys.index("id")))

    #Write to CSV file
    if debug: print "Writing people data to CSV file"
    f = open(csv_filepath,"wb")
    w = unicodecsv.writer(f, encoding='utf-8')
    w.writerow(keys)

    for person in people_list:
        row = [person.get(k,'') for k in keys]
        w.writerow(row)

    f.close()
    if debug: print "Finished writing people data to CSV file"
