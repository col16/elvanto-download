import ElvantoAPI
import people
import groups
import reports
import misc
import os
import datetime


def standard_api_download(settings, output_directory):
    """ Download default collection of data from API """
    connection = ElvantoAPI.Connection(APIKey=settings.API_key)

    #Dump all People data to CSV
    people.save_to_csv(
        connection,
        os.path.join(output_directory, "people.csv"),
        settings.people_custom_fields)

    #Dump all Group data to CSV
    groups.save_to_csv(
        connection,
        output_filepaths = {
            'group': os.path.join(output_directory, "groups.csv"),
            'membership': os.path.join(output_directory, "group_membership.csv"),
            'group_category_memberships': os.path.join(output_directory, "group_categories.csv"),
            'group_category_names': os.path.join(output_directory, "group_category_names.csv"),
        }
    )   



def standard_report_download(settings, output_directory):
    """ Download default collection of reports """

    #Log in to Elvanto
    session = reports.log_in_to_elvanto(settings.username, settings.password)

    today = datetime.date.today()

    #People notes report
    print "Downloading People Notes"
    reports.get_report(session,
        type = 'people_notes',
        params = {
            #'date_start': today - datetime.timedelta(days=6),
            #'date_end': today + datetime.timedelta(days=1),
            'date_range_type': 'last_7_days',
            },
        output_filename = os.path.join(output_directory,
            'people_notes_last_7_days.csv'),
        )

    #Service attendance report
    print "Downloading Service Attendance"
    for service in settings.services:
        reports.get_report(session,
            type = 'service_attendance',
            params = {
                'date_range_type': 'last_7_days',
                'location_id': service['location_id'],
                'service_type_ids': service['service_type_ids'],
                },
            output_filename = os.path.join(output_directory,
                'service_attendance_last_7_days_'+\
                misc.slugify(service['name'])+'.csv'),
            )
