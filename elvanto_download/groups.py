import unicodecsv
import misc


def get_all_from_elvanto(connection):

    have_all = False
    page = 1
    page_size = 1000
    group_list = []

    params = {
        'page_size':page_size,
        'fields': [
            'people',
            'categories',
            ]
        }

    while not have_all:

        params['page'] = page
        data = connection._Post("groups/getAll", **params)

        if not ('status' in data and 'groups' in data):
            return None
        if not data['status'] == "ok":
            return None

        for g in data['groups'].get('group',[]):
            group_list.append(g)

        if data['groups'].get('total',0) <= page * page_size:
            have_all = True
        else:
            page += 1

    return group_list


def save_to_csv(connection, output_filepaths, debug=True):
    
    #Get data from Elvanto
    if debug: print "Downloading group data"
    group_list = get_all_from_elvanto(connection)

    if debug: print "Writing group data to CSV file"

    ## First task - create CSV file to record group membership (i.e. people IDs
    #against group IDs).
    f = open(output_filepaths['membership'],"wb")
    w = unicodecsv.writer(f, encoding='utf-8')
    w.writerow(['group_id','person_id'])

    for group in group_list:
        group_id = group.get('id','')
        if "people" in group:
            people = misc.get_special_list(group,'people','person')
            for p in people:
                person_id = p.get('id','')
                w.writerow([group_id,person_id])
            group.pop("people",None)

    f.close()

    ## Second task - create CSV file to record group categories (i.e. group IDs
    #against category IDs).
    f1 = open(output_filepaths['group_category_memberships'],"wb")
    w1 = unicodecsv.writer(f1, encoding='utf-8')
    w1.writerow(['group_id','group_category_id'])
    f2 = open(output_filepaths['group_category_names'],"wb")
    w2 = unicodecsv.writer(f2, encoding='utf-8')
    w2.writerow(['group_category_id','group_category_name'])
    known_categories = {}
    for group in group_list:
        group_id = group.get('id','')
        if "categories" in group:
            categories = misc.get_special_list(group,'categories','category')
            for c in categories:
                category_id = c.get('id','')
                category_name = c.get('name','')
                w1.writerow([group_id, category_id])
                if category_id not in known_categories:
                    w2.writerow([category_id, category_name])
                    known_categories[category_id] = category_name
            group.pop("categories",None)
    f1.close()
    f2.close()

    ## Third task - create CSV with data on groups themselves.
    #Get list of all keys that are used
    keys = []
    for group in group_list:
        for k in group.keys():
            if k not in keys:
                keys.append(k)
    keys.sort()
    #Move ID and name fields to start of key-list
    keys.insert(0, keys.pop(keys.index("name")))
    keys.insert(0, keys.pop(keys.index("id")))

    #Write to CSV file
    f = open(output_filepaths['group'],"wb")
    w = unicodecsv.writer(f, encoding='utf-8')
    w.writerow(keys)

    for group in group_list:
        row = [group.get(k,'') for k in keys]
        w.writerow(row)

    f.close()
    if debug: print "Finished writing group data to CSV file"
