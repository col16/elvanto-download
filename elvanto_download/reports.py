import requests
import os


base_url = "https://cornerstone.elvanto.com.au/"
base_headers = {'User-Agent': 'Mozilla/5.0'} #Mimic a known browser
chunk_size = 100*1024 #100kb


def log_in_to_elvanto(username, password):
    """ Log into Elvanto using Session object that stores cookies """
    session = requests.Session()
    response = session.post(
            os.path.join(base_url,"login/"), 
            data = {
                'login_username': username,
                'login_password': password,
                #'redirect_to': report_url,
                },
            headers = base_headers
        )
    if not 200 <= response.status_code < 300:
        raise Exception("Error %d while logging in" % response.status_code)
    else:
        return session


def get_report(session, type, params, output_filename, debug=False):
    """
    Save Elvanto report 'type' with required parameters 'params' to file
    Pass type='url' and params with 'url' key in order to download report
    manually
    """
    if type == 'url':
        report_url = params['url']
    elif type == 'service_attendance':
        report_url = service_attendance_url(**params)
    elif type == 'people_notes':
        report_url = people_notes_url(**params)
    else:
        raise Exception("Unknown report type")

    r = session.get(
        report_url,
        headers = base_headers,
        stream = True,
        )

    if debug:
        print r.url

    if r.status_code == 200:
        with open(output_filename, 'wb') as f:
            for chunk in r.iter_content(chunk_size):
                f.write(chunk)
    else:
        raise Exception("Error %d while downloading report from %s" % \
            r.status_code, r.url)


def date_string(date):
    if date:
        return '{:02d}%2F{:02d}%2F{:4d}'.format(date.day,date.month,date.year)
    else:
        return ''


def service_attendance_url(location_id, service_type_ids,
    date_start=None, date_end=None, date_range_type=""):

    url = base_url +\
        "admin/reports/report_existing/" +\
        "?category=services" +\
        "&report=service_reports_individuals" +\
        "&date_range_type="+date_range_type +\
        "&date_range_from="+date_string(date_start) +\
        "&date_range_to="+date_string(date_end) +\
        "&frequency=" +\
        "&attendance=attended" +\
        "&did_not_attend_times=" +\
        "&attended_times=" +\
        "&service_types_services=y" +\
        "&locations_services=y" +\
        "&location%5B%5D="+location_id +\
        "&multi_select_filter_demographic%5B%5D=" +\
        "&multi_select_filter_columns%5B%5D=" +\
        "&columns%5B%5D=member_id" +\
        "&sort=1" +\
        "&order=asc" +\
        "&output=csv" +\
        "&export_orientation=" +\
        "&export_size=" +\
        "&export_labels_address="

    for service_id in service_type_ids:
        url += "&service_type%5B%5D="+service_id

    return url

def people_notes_url(date_start=None, date_end=None, date_range_type=""):
    return base_url +\
        "admin/reports/report_existing/" +\
        "?category=people" +\
        "&report=people_notes" +\
        "&show_notes=" +\
        "&show_notes_status=" +\
        "&date_range_type="+date_range_type +\
        "&date_range_from="+date_string(date_start) +\
        "&date_range_to="+date_string(date_end) +\
        "&what_notes=all" +\
        "&search_person=" +\
        "&note_contains=" +\
        "&multi_select_filter_demographic%5B%5D=" +\
        "&multi_select_filter_columns%5B%5D=" +\
        "&columns%5B%5D=member_id" +\
        "&sort=0" +\
        "&order=asc" +\
        "&output=csv" +\
        "&export_orientation=" +\
        "&export_size=" +\
        "&export_labels_address="
