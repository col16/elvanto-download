from setuptools import setup

setup(name='elvanto-download',
      version='0.4',
      description='Download data from Elvanto',
      url='https://bitbucket.org/col16/elvanto-download/',
      author='Cameron Oliver',
      author_email='cameron.oliver@gmail.com',
      license='MIT',
      packages = [
        'elvanto_download',
      ],
      install_requires=[
          'unicodecsv==0.13.0',
          'ElvantoAPI==1.3.3.2',
          'requests==2.10.0',
      ],
      zip_safe=True)
