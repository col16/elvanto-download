#== Elvanto username and password to use when downloading reports ==#
username = ''
password = ''


#== API parameters ==#
API_key = ''

people_custom_fields = [
    # You can obtain the ID of a custom field by going to a People Category Layout
    # (https://cornerstone.elvanto.com.au/admin/people/categories/) and editing
    # the Custom Field.
    ['bd73135d-8406-11e4-96cb-0622f7688f16', 'Access to Talks'],
    ['5e5c69bd-bae7-11e4-a825-06ba798128be', 'Alternative email address'],
    ['42779e7c-b2f2-11e3-8b73-bb41139cd456', 'Assigned Staff Member'],
    ['4c48249d-ce7b-11e4-bb6c-06ba798128be', 'Baptism date'],
    ['6dada8ad-5b09-11e4-a869-0622f7688f16', 'Bible Study Attendance'],
    ['743aeafa-b2e7-11e3-8b73-bb41139cd456', 'Campus Group'],
    ['ea19f1a4-ce7f-11e3-b4bf-33ca5b67f598', 'CCB Member ID'],
    ['16d1a66d-fe7e-11e4-bb6c-06ba798128be', 'Childwork Ministry Guidelines Signed'],
    ['accbc04c-dc80-11e3-b2fb-06cb09f7b8af', 'Completed On Board Course'],
    ['26867321-206c-11e5-bb6c-06ba798128be', 'Conferences 2015'],
    ['3aa5d498-b2f2-11e3-8b73-bb41139cd456', 'Contacted By'],
    ['3643f4f8-d0b5-11e3-803f-5ec2b131e0cf', 'Cornerstone Giving Number'],
    ['651a179e-b2f1-11e3-8b73-bb41139cd456', 'Dietary Needs'],
    ['9fe83095-2744-11e4-91c4-06d3d709c2b0', 'Doing One-to-One with'],
    ['9181bcf3-fb29-11e3-91c4-06d3d709c2b0', 'Donor Type'],
    ['438e746e-532b-11e4-a869-0622f7688f16', 'Final Semester Of Study (expected)'],
    ['5c1e5ea2-b2e7-11e3-8b73-bb41139cd456', 'Hall of Residence'],
    ['039f5cb8-532c-11e4-a869-0622f7688f16', 'Hall of Residence Selection'],
    ['bea6cdac-b2f1-11e3-8b73-bb41139cd456', 'Home Town'],
    ['67a28617-fe7e-11e4-bb6c-06ba798128be', 'Lanyards Issued'],
    ['065b6c52-4827-11e4-a869-0622f7688f16', 'Name Tag'],
    ['b0e94870-b2f1-11e3-8b73-bb41139cd456', 'Other Church(es) Attending'],
    ['83259cbb-58c1-11e4-a869-0622f7688f16', 'Police Vetted Date'],
    ['70ba2b11-58c1-11e4-a869-0622f7688f16', 'Police Vetting Status'],
    ['65dd4fc0-b2e7-11e3-8b73-bb41139cd456', 'Supporter Type'],
    ['2b43c1e3-532c-11e4-a869-0622f7688f16', 'University Course Selection'],
    ['9227ef68-b2e7-11e3-8b73-bb41139cd456', 'University Course/Degree'],
    ['27237db6-c5ea-11e3-b4bf-33ca5b67f598', 'Work Phone Number'],
]
